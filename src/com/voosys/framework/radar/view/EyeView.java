package com.voosys.framework.radar.view;

import com.voosys.framework.radar.math3d.Math3dUtil;
import com.voosys.framework.radar.math3d.Trig1;
import com.voosys.framework.radar.math3d.Trig3;
import com.voosys.framework.radar.math3d.Vector1;
import com.voosys.framework.radar.math3d.Vector2;
import com.voosys.framework.radar.math3d.Vector3;
import com.voosys.framework.radar.point.Point;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class EyeView extends ARView {

	private static final int SCREEN_DEPTH = 1;
	private Vector3 camRot                = new Vector3();
	private Trig3   camTrig               = new Trig3();
	private Vector3 camPos                = new Vector3();
	private Vector3 pointPos              = new Vector3();
	private Vector3 relativePos           = new Vector3();
	private Vector3 relativeRotPos        = new Vector3();
	private Vector3 screenRatio           = new Vector3();	
	private Vector2 screenPos             = new Vector2();
	private Vector2 screenSize            = new Vector2();
	private Vector1 screenRot             = new Vector1();
	private Trig1 	screenRotTrig         = new Trig1();
	private boolean drawn;
	
	public EyeView(Context context) {
		
		super(context);		
		init();
		
	}

	public EyeView(Context context, AttributeSet attrs) {
		
		super(context, attrs);
		init();
		
	}

	EyeView(Context context, AttributeSet attrs, int defStyle) {
		
		super(context, attrs, defStyle);
		init();
		
	}
	
	private void init() {
		
		screenRatio.z = SCREEN_DEPTH;
		
	}

	@Override
    protected void preRender(Canvas canvas) {
		
			screenRatio.y = (getWidth()+getHeight())/2;
            screenRatio.x = (getWidth()+getHeight())/2;
            screenSize.y  = getHeight();
            screenSize.x  = getWidth();
            
            Math3dUtil.getCamRotation(getOrientation(), getPhoneRotation(), camRot, camTrig, screenRot, screenRotTrig);
			Math3dUtil.convertLocationToPosition(getLocation(),  camPos);
			
    }

	@Override
	protected void calculatePointCoordinates(Point point) {
		
		Math3dUtil.convertLocationToPosition(point.getLocation(), pointPos);
		Math3dUtil.getRelativeTranslationInMeters(pointPos, camPos, relativePos);
		Math3dUtil.getRelativeRotation(relativePos, camTrig, relativeRotPos);
		
        drawn = Math3dUtil.convert3dTo2d(relativeRotPos, screenSize, screenRatio,  screenRotTrig, screenPos);
        
        if (drawn) {
        	
	        point.setX((float) screenPos.x);
		    point.setY((float) screenPos.y);
		    
        }
        
        point.setDrawn(drawn);
        
}

	@Override
	protected void postRender(Canvas canvas) {
		
	}	
	
}