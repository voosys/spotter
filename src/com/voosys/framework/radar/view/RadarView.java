package com.voosys.framework.radar.view;

import com.voosys.spotter.R;
import com.voosys.framework.radar.point.Point;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class RadarView extends ARView {

	private int rotableBackground = 0;
	private double compassAngle   = 0;
	private int mScanRadius       = 5;
	private float center;
	private Bitmap rotableBacgkroundBitmap;
	private Dialog mRadiusDialog;
		
	public RadarView(Context context) {
		
		super(context);
		
	}

	public RadarView(Context context, AttributeSet attrs) {
		
		super(context, attrs);
		
	}

	RadarView(Context context, AttributeSet attrs, int defStyle) {
		
		super(context, attrs, defStyle);
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		
		int measuredWidth  = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
		int measuredHeight = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
		int size           = Math.max(measuredWidth, measuredHeight);
		center             = size / 2;
		
		setMeasuredDimension(size, size);
		
	}

	@Override
	protected void calculatePointCoordinates(Point point) {

		double pointAngle    = getAngle(point) + compassAngle;
		double pixelDistance = point.getDistance() * center / getMaxDistance();
		double pointy        = center - pixelDistance * Math.sin(pointAngle);
		double pointx        = center + pixelDistance * Math.cos(pointAngle);
		
		point.setX((float) pointx);
		point.setY((float) pointy);
		
	}

	@Override
	protected void preRender(Canvas canvas) {
		
		drawBackground(canvas);
		
		compassAngle = getOrientation().getY();

	}

	@Override
	protected void postRender(Canvas canvas) {
		
		Paint pointPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		
		pointPaint.setColor(0xff00a0d2);
		canvas.drawCircle(center, center, 5, pointPaint);

	}

	private void drawBackground(Canvas canvas) {
		
		if (getRotableBackground() != 0 && getOrientation()!=null) {

			Matrix transform = new Matrix();
			
			transform.setRectToRect(new RectF(0, 0, rotableBacgkroundBitmap.getWidth(), rotableBacgkroundBitmap.getHeight()), new RectF(0, 0, getWidth(), getWidth()), Matrix.ScaleToFit.CENTER);
			transform.preRotate((float) -(Math.toDegrees(compassAngle)), rotableBacgkroundBitmap.getWidth() / 2, rotableBacgkroundBitmap.getHeight() / 2);
			canvas.drawBitmap(rotableBacgkroundBitmap, transform, null);
			
		}
		
	}
	
	public int getRotableBackground() {
		
		return rotableBackground;
		
	}

	public void setRotableBackground(int rotableBackground) {
		
		this.rotableBackground  = rotableBackground;		
		rotableBacgkroundBitmap = BitmapFactory.decodeResource(this.getResources(), rotableBackground);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		mRadiusDialog = new Dialog(this.getContext());
		
		mRadiusDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mRadiusDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		mRadiusDialog.setContentView(R.layout.radius);
		mRadiusDialog.setCanceledOnTouchOutside(true);
		
		final SeekBar mRadius      = (SeekBar) mRadiusDialog.findViewById(R.id.scanRadius);
		final TextView radiusValue = (TextView) mRadiusDialog.findViewById(R.id.scanRadiusValue);
		
		mRadius.setProgress(this.mScanRadius);
		radiusValue.setText("" + mScanRadius + " Km");
		mRadius.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        		
            	if (progress < 1) {
            		
            		progress = 1;
            		
            	}
            	
            	if (progress > 10) {
            		
            		progress = 10;
            		
            	}
            	
            	mScanRadius = progress;
            	
            	mRadius.setProgress(mScanRadius);
            	radiusValue.setText("" + mScanRadius + " Km");            	
            	
            }
 
            public void onStartTrackingTouch(SeekBar seekBar) {
                
            }
 
            public void onStopTrackingTouch(SeekBar seekBar) {
                
            }
            
        });
		
		mRadiusDialog.show();
		
		return false;
		
	}
	
	public int getScanRadius() {
		
		return this.mScanRadius;
		
	}

}