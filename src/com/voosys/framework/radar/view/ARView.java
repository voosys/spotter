package com.voosys.framework.radar.view;

import java.util.List;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.PointsUtil;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.content.Context;
import android.graphics.Canvas;
import android.location.Location;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public abstract class ARView extends View {
	
	public interface OnPointPressedListener {
		
		public void onPointPressed(Point p);
		
	}

	private static final double DEFAULT_MAX_DISTANCE = 1000;

	private Location location;
	
	private double maxDistance = DEFAULT_MAX_DISTANCE;
	
	private List<? extends Point> points;

	private OnPointPressedListener onPointPressedListener;

	private Double minorDistance;

	private double distance;

	private Point p;

	private PointRenderer pointRenderer;
	
	private Orientation orientation;
	
	private int phoneRotation = 0;

	public ARView(Context context) {
		
		super(context);
		
	}

	public ARView(Context context, AttributeSet attrs) {
		
		super(context, attrs);
		
	}

	public ARView(Context context, AttributeSet attrs, int defStyle) {
		
		super(context, attrs, defStyle);
		
	}

	protected Point findNearestPoint(float x, float y) {
		
		p             = null;
		minorDistance = (double) Math.max(this.getWidth(),this.getHeight());
		
		for (Point point : getpoints()) {
			
			distance = Math.sqrt(Math.pow((point.getX() - x), 2) + Math.pow((point.getY() - y), 2));
			
			if (distance < minorDistance) {
				
				minorDistance = distance;
				p             = point;
				
			}
			
		}
		
		return p;
		
	}

	@Override
	protected void onDraw(Canvas canvas) {
		
		super.onDraw(canvas);
		
		if (getOrientation()==null) {
			
			return;
			
		}
		
		PointsUtil.calculateDistance(points, location);
		preRender(canvas);
		
		if (getpoints() != null) {
			
			for (Point point : getpoints()) {
				
				calculatePointCoordinates(point);
				
				if (point.getDistance()<maxDistance && point.isDrawn()) {
					
					if (point.getRenderer()!=null) {
						
						point.getRenderer().drawPoint(point, canvas, orientation);						
						
					} else {
						
						if (this.pointRenderer!=null) {
							
							getPointRenderer().drawPoint(point, canvas, orientation);
							
						}
						
					}
					
				}
				
			}
			
		}
		
		postRender(canvas);
		
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			
			Point p = findNearestPoint(event.getX(), event.getY());
			
			if (p != null && getOnPointPressedListener() != null) {
				
				if (Math.abs(p.getX()-event.getX())<50 && Math.abs(p.getY()-event.getY())<50) {
					
					onPointPressedListener.onPointPressed(p);
					
				}
				
			}
			
		}
		
		return super.onTouchEvent(event);
		
	}

	protected abstract void preRender(Canvas canvas);

	protected abstract void calculatePointCoordinates(Point point);

	protected abstract void postRender(Canvas canvas);

	protected double getAngle(Point point) {
		
		return Math.atan2(point.getLocation().getLatitude() - location.getLatitude(), point.getLocation().getLongitude() - location.getLongitude());
		
	}

	protected double getVerticalAngle(Point point) {
		
		return Math.atan2(point.getDistance(), location.getAltitude());
		
	}

	public Location getLocation() {
		
		return location;
		
	}


	public double getMaxDistance() {
		
		return maxDistance;
		
	}

	public OnPointPressedListener getOnPointPressedListener() {
		
		return onPointPressedListener;
		
	}

	protected List<? extends Point> getpoints() {
		
		return points;
		
	}

	public void setLocation(Location location) {
		
		this.location = location;
		
	}

	public void setMaxDistance(double maxDistance) {
		
		this.maxDistance = maxDistance;
		
		this.invalidate();

	}

	public void setOnPointPressedListener(OnPointPressedListener onPointPressedListener) {
		
		this.onPointPressedListener = onPointPressedListener;
		
	}

	public void setPosition(Location location) {
		
		this.location = location;
		
		PointsUtil.calculateDistance(points,location);
		
	}

	public void setPoints(List<? extends Point> points) {
		
		this.points = points;
		
	}

	public PointRenderer getPointRenderer() {
		
		return pointRenderer;
		
	}

	public void setPointRenderer(PointRenderer pointRenderer) {
		
		this.pointRenderer = pointRenderer;		

	}

	public Orientation getOrientation() {
		
		return orientation;
		
	}

	public void setOrientation(Orientation orientation) {
		
		this.orientation = orientation;
		
		this.invalidate();

	}

	public int getPhoneRotation() {
		
		return phoneRotation;
		
	}

	public void setPhoneRotation(int phoneRotation) {
		
		this.phoneRotation = phoneRotation;
		
	}

}