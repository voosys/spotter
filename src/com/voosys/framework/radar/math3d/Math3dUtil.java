package com.voosys.framework.radar.math3d;

import com.voosys.framework.radar.orientation.Orientation;

import android.location.Location;
import android.view.Surface;

public class Math3dUtil {

	private static final double METERS_IN_A_DEGREE = 111111;	
	private static double QUADRANT                 = Math.PI / 2;
	private static Vector2 viewPortPos             = new Vector2();

	public static void getCamRotation(Orientation inOrientation, int inPhoneRotation, Vector3 outCamRot, Trig3 outCamTrig, Vector1 outScreenRot, Trig1 outScreenRotTrig) {
		
		outCamRot.x = -inOrientation.getX();
		outCamRot.y = inOrientation.getY()+Math.PI;
		outCamRot.z = 0;
		
		if (inPhoneRotation == Surface.ROTATION_0) {
			
			outScreenRot.v = -inOrientation.getZ();
			
		}
		
		if (inPhoneRotation == Surface.ROTATION_180) {
			
			outScreenRot.v = -inOrientation.getZ() + Math.PI;
			
		}

		if (inPhoneRotation == Surface.ROTATION_90) {
			
			outScreenRot.v = -inOrientation.getZ()-QUADRANT;
			
		}
		
		if (inPhoneRotation == Surface.ROTATION_270) {
			
			outScreenRot.v = -inOrientation.getZ()+QUADRANT;
			
		}
		
		outCamTrig.setVector3(outCamRot);
		outScreenRotTrig.setVector1(outScreenRot);

	}

	public static void convertLocationToPosition(Location inLocation, Vector3 outPos) {
		
		outPos.z = inLocation.getLatitude();
		outPos.x = inLocation.getLongitude();
		outPos.y = inLocation.getAltitude();
		
	}

	public static void getRelativeTranslationInMeters(Vector3 inPointPos, Vector3 inCamPos,  Vector3 outRelativePosInMeters) {
		
		outRelativePosInMeters.z = (inCamPos.z-inPointPos.z) * METERS_IN_A_DEGREE;
		outRelativePosInMeters.y = (inCamPos.y-inPointPos.y);
		outRelativePosInMeters.x = (inCamPos.x-inPointPos.x) * Math.cos(inCamPos.z-inPointPos.z) * METERS_IN_A_DEGREE;
		
	}
	
	public static void getRelativeRotation(Vector3 inRelativePos, Trig3 inCamTrig, Vector3 outRelativeRotPos) {
		
        outRelativeRotPos.x = inCamTrig.yCos * (inCamTrig.zSin * inRelativePos.y + inCamTrig.zCos * inRelativePos.x) - inCamTrig.ySin * inRelativePos.z;
        outRelativeRotPos.y = inCamTrig.xSin * (inCamTrig.yCos * inRelativePos.z + inCamTrig.ySin * (inCamTrig.zSin * inRelativePos.y + inCamTrig.zCos * inRelativePos.x)) + inCamTrig.xCos * (inCamTrig.zCos * inRelativePos.y - inCamTrig.zSin * inRelativePos.x);
        outRelativeRotPos.z = inCamTrig.xCos * (inCamTrig.yCos * inRelativePos.z + inCamTrig.ySin * (inCamTrig.zSin * inRelativePos.y + inCamTrig.zCos * inRelativePos.x)) - inCamTrig.xSin * (inCamTrig.zCos * inRelativePos.y - inCamTrig.zSin * inRelativePos.x);
        
	}

	public static boolean convert3dTo2d(Vector3 inRelativePos, Vector2 inScreenSize, Vector3 inScreenRatio, Trig1 inScreenRotTrig, Vector2 outScreenPos) {
	
		if (inRelativePos.z > 0) {
			
	    	viewPortPos.x  = (inRelativePos.x * inScreenRatio.x) / (inScreenRatio.z * inRelativePos.z);
	    	viewPortPos.y  = (inRelativePos.y * inScreenRatio.y) / (inScreenRatio.z * inRelativePos.z);
		    outScreenPos.x = inScreenSize.x / 2 +  viewPortPos.x * inScreenRotTrig.cos - viewPortPos.y * inScreenRotTrig.sin;
		    outScreenPos.y = inScreenSize.y /2 +  viewPortPos.y * inScreenRotTrig.cos + viewPortPos.x * inScreenRotTrig.sin;
		    
		    return true;
		    
		} else {
			
			return false;
			
		}	
		
	}
	
}
