package com.voosys.framework.radar.point;

import java.util.ArrayList;
import java.util.List;

import android.location.Location;

public class PointsUtil {
	
	private static int EARTH_RADIUS_METERS = 6371000;
	
	/*
	* calculateDistance method
	* @param List<Point> points, Location location
	* @return NULL
	* @description calculateDistance method
	*/
	public static void calculateDistance(List<? extends Point> points, Location location) {
		
		for (Point poi : points) {
			
			poi.setDistance(distanceInMeters(poi.getLocation(),location));
			
		}
		
	}
	
	/*
	* getNearPoints method
	* @param List<Point> points, Location location, double distance
	* @return List<Point>
	* @description getNearPoints method
	*/
	public static List<Point> getNearPoints(List<Point> points, Location location, double distance) {
		
		calculateDistance(points, location);
		
		List<Point> subPoints = new ArrayList<Point>();
		
		for (Point poi : points) {
			
			if (poi.getDistance() <= distance) {
				
				subPoints.add(poi);
				
			}
			
		}
		
		return subPoints;
		
	}
	
	/*
	* distanceInMeters method
	* @param Location location, Location otherLocation
	* @return double
	* @description distanceInMeters method
	*/
	private static double distanceInMeters(Location location, Location otherLocation) {
		
		double lat1Rad     = Math.toRadians(otherLocation.getLatitude());
		double lat2Rad     = Math.toRadians(location.getLatitude());
		double deltaLonRad = Math.toRadians(location.getLongitude() - otherLocation.getLongitude());

		return Math.acos(Math.sin(lat1Rad) * Math.sin(lat2Rad) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.cos(deltaLonRad)) * EARTH_RADIUS_METERS;
		
	}

}