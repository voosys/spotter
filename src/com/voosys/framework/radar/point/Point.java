package com.voosys.framework.radar.point;

import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.location.Location;

public interface Point {

	public abstract int getId();
	public abstract void setId(int id);
	public abstract String getTitle();
	public abstract void setTitle(String title);
	public abstract String getType();
	public abstract void setType(String type);
	public abstract String getUrl();	
	public abstract void setUrl(String url);	
	public abstract Location getLocation();
	public abstract void setLocation(Location location);
	public abstract double getDistance();
	public abstract void setDistance(double distance);	
	public abstract PointRenderer getRenderer();
	public abstract void setRenderer(PointRenderer renderer);
	public abstract float getX();
	public abstract void setX(float x);
	public abstract float getY();
	public abstract void setY(float y);
	public abstract String getReference();	
	public abstract void setReference(String reference);	
	public abstract boolean isSelected();	
	public abstract void setSelected(boolean selected);	
	public abstract boolean isDrawn();	
	public abstract void setDrawn(boolean drawn);	
	public abstract boolean isHidden();
	public abstract void hide();
	
}