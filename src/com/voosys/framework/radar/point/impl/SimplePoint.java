package com.voosys.framework.radar.point.impl;

import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.location.Location;

public class SimplePoint implements Point {

	public static int MILLION = 1000000;	
	private boolean drawn     = true;
	private boolean hide      = false;
	private int id;
	private Location location;
	private double distance;
	private String title;
	private String type;
	private PointRenderer renderer;
	private float x;
	private float y;
	private boolean selected;
	private String url;
	private String reference;
	
	/*
	* isSelected method
	* @param NULL
	* @return boolean
	* @description isSelected method
	*/
	public boolean isSelected() {
		
		return selected;
		
	}
	
	/*
	* setSelected method
	* @param boolean selected
	* @return NULL
	* @description setSelected method
	*/
	public void setSelected(boolean selected) {
		
		this.selected = selected;
		
	}
	
	/*
	* Constructor
	* @param int id, Location location, PointRenderer renderer, String title, String type, String url, double distance, String reference
	* @return NULL
	* @description Class constructor
	*/
	public SimplePoint(int id, Location location, PointRenderer renderer, String title, String type, String url, double distance, String reference) {
		
		super();		
		this.setLocation(location);
		
		this.renderer  = renderer;
		this.title     = title;
		this.type      = type;
		this.url       = url;	
		this.distance  = distance;
		this.reference = reference;
		
	}
	
	/*
	* Constructor
	* @param int id, Location location, PointRenderer renderer
	* @return NULL
	* @description Class constructor
	*/
	public SimplePoint(int id, Location location, PointRenderer renderer) {
		
		this(id, location, renderer, "", "", "", 0.0, "");
		
	}
	
	/*
	* Constructor
	* @param int id, Location location
	* @return NULL
	* @description Class constructor
	*/
	public SimplePoint(int id, Location location) {
		
		this(id, location, null);
		
	}
	
	/*
	* getDistance method
	* @param NULL
	* @return double
	* @description getDistance method
	*/
	@Override
	public double getDistance() {
		
		return distance;
		
	}
	
	/*
	* setDistance method
	* @param double distance
	* @return NULL
	* @description setDistance method
	*/
	@Override
	public void setDistance(double distance) {
		
		this.distance = distance;
		
	}
	
	/*
	* getTitle method
	* @param NULL
	* @return String
	* @description getTitle method
	*/
	@Override
	public String getTitle() {
		
		return title;
		
	}
	
	/*
	* setTitle method
	* @param String title
	* @return NULL
	* @description setTitle method
	*/
	@Override
	public void setTitle(String title) {
		
		this.title = title;
		
	}
	
	/*
	* getType method
	* @param NULL
	* @return String
	* @description getType method
	*/
	@Override
	public String getType() {
		
		return type;
		
	}
	
	/*
	* setType method
	* @param String type
	* @return NULL
	* @description setType method
	*/
	@Override
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	/*
	* getRenderer method
	* @param NULL
	* @return PointRenderer
	* @description getRenderer method
	*/
	@Override
	public PointRenderer getRenderer() {
		
		return renderer;
		
	}
	
	/*
	* setRenderer method
	* @param PointRenderer renderer
	* @return NULL 
	* @description setRenderer method
	*/
	@Override
	public void setRenderer(PointRenderer renderer) {
		
		this.renderer = renderer;
		
	}
	
	/*
	* getId method
	* @param NULL
	* @return int 
	* @description setRenderer method
	*/
	@Override
	public int getId() {
		
		return id;
		
	}
	
	/*
	* setId method
	* @param int id
	* @return NULL
	* @description setRenderer method
	*/
	@Override
	public void setId(int id) {
		
		this.id = id;
		
	}
	
	
	/*
	* getX method
	* @param NULL
	* @return float 
	* @description getX method
	*/
	@Override
	public float getX() {
		
		return x;
		
	}
	
	/*
	* setX method
	* @param float x
	* @return NULL
	* @description setX method
	*/
	@Override
	public void setX(float x) {
		
		this.x = x;
		
	}
	
	/*
	* getY method
	* @param NULL
	* @return float 
	* @description getY method
	*/
	@Override
	public float getY() {
		
		return y;
		
	}
	
	/*
	* setY method
	* @param float y
	* @return NULL
	* @description setY method
	*/
	@Override
	public void setY(float y) {
		
		this.y = y;
		
	}
	
	/*
	* getLocation method
	* @param NULL
	* @return Location 
	* @description getLocation method
	*/
	@Override
	public Location getLocation() {
		
		return location;
		
	}
	
	/*
	* setLocation method
	* @param Location location
	* @return NULL
	* @description setLocation method
	*/
	@Override
	public void setLocation(Location location) {
		
		this.location = location;
		
	}
	
	/*
	* isDrawn method
	* @param NULL
	* @return boolean 
	* @description isDrawn method
	*/
	@Override
	public boolean isDrawn() {
		
		return drawn;
		
	}
	
	/*
	* setDrawn method
	* @param boolean drawn
	* @return NULL
	* @description setDrawn method
	*/
	@Override
	public void setDrawn(boolean drawn) {
		
		this.drawn=drawn;
		
	}
	
	/*
	* getUrl method
	* @param NULL
	* @return String 
	* @description getUrl method
	*/
	@Override
	public String getUrl() {
		
		return url;
		
	}
	
	/*
	* setUrl method
	* @param String url
	* @return NULL
	* @description setUrl method
	*/
	@Override
	public void setUrl(String url) {
		
		this.url = url;
		
	}
	
	/*
	* getReference method
	* @param NULL
	* @return String 
	* @description getReference method
	*/
	@Override
	public String getReference() {
		
		return reference;
		
	}

	/*
	* setReference method
	* @param String reference
	* @return NULL
	* @description setReference method
	*/
	@Override
	public void setReference(String reference) {
		
		this.reference = reference;
		
	}
	
	/*
	* hide method
	* @param NULL
	* @return NULL
	* @description hide method
	*/
	@Override
	public void hide() {
		
		this.hide = true;
		
	}
	
	/*
	* isHidden method
	* @param NULL
	* @return boolean
	* @description isHidden method
	*/
	public boolean isHidden() {
		
		return hide;
		
	}

}