package com.voosys.framework.radar.point.renderer.impl;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class AugmentedDrawablePointRenderer implements PointRenderer {
	
	private Bitmap b = null;
	private Paint pText;
	private Paint pBlackLine;	
	private Paint pCircle;
	
	public AugmentedDrawablePointRenderer() {
		
	}

	@Override
	public void drawPoint(Point point, Canvas canvas, Orientation orientation) {
		
		if (b == null) {
			
			pText = new Paint(Paint.ANTI_ALIAS_FLAG);
			
			pText.setStyle(Paint.Style.STROKE);
			pText.setTextAlign(Paint.Align.CENTER);
			pText.setTextSize(20);
			pText.setTypeface(Typeface.SANS_SERIF);
			pText.setColor(Color.WHITE);
			
			pCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
			
			pCircle.setStyle(Paint.Style.STROKE);
			pCircle.setStrokeWidth(4);
			pCircle.setColor(Color.WHITE);
			
			pBlackLine=new Paint(Paint.ANTI_ALIAS_FLAG);
			
			pBlackLine.setColor(Color.BLACK);
			pBlackLine.setTextSize(20);
			pBlackLine.setTypeface(Typeface.SANS_SERIF);
			pBlackLine.setTextAlign(Paint.Align.CENTER);
		
		}
	
		float size = (float)((10 - point.getDistance()) * 6);
		
		canvas.drawCircle(point.getX(), point.getY(), (float) size, pCircle);
		
		float textWidth = pText.breakText(point.getTitle(), true, 500, null)/2;
		
		canvas.drawText(point.getTitle(), point.getX() - textWidth + 2, point.getY() + size + 16, pBlackLine);
		canvas.drawText(point.getTitle(), point.getX() - textWidth , point.getY() + size + 14, pText);
		
	}

}