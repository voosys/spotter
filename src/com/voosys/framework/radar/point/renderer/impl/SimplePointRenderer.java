package com.voosys.framework.radar.point.renderer.impl;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class SimplePointRenderer implements PointRenderer {

	private static Bitmap b = null;
	
	@Override
	public void drawPoint(Point point, Canvas canvas, Orientation orientation) {
		
		if (b == null) {
			
			Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
			
			p.setColor(0x44FFFFFF);
			canvas.drawCircle(point.getX(), point.getY(), 5, p);
			p.setColor(0x33FFFFFF);
			canvas.drawCircle(point.getX(), point.getY(), 4, p);
			p.setColor(0x66FFFFFF);
			canvas.drawCircle(point.getX(), point.getY(), 3, p);
			p.setColor(0x99FFFFFF);
			canvas.drawCircle(point.getX(), point.getY(), 2, p);
			p.setColor(0xFFFFFFFF);
			canvas.drawCircle(point.getX(), point.getY(), 1, p);
			
		}
		
	}
	
}