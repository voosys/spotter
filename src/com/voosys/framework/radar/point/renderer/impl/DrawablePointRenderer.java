package com.voosys.framework.radar.point.renderer.impl;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

public class DrawablePointRenderer implements PointRenderer {
	
	private Bitmap b=null;
	private Resources res;
	private int id;
	private int xOff;
	private int yOff;
	private Paint pText;
	
	public DrawablePointRenderer(Resources res, int id) {
		
		this.id  = id;
		this.res = res;
		
	}
	
	@Override
	public void drawPoint(Point point, Canvas canvas, Orientation orientation) {
		
		if (b == null) {
			
			b     = BitmapFactory.decodeResource(res, id);
			xOff  = b.getWidth()/2;
			yOff  = b.getHeight()/2;
			pText = new Paint(Paint.ANTI_ALIAS_FLAG);
			
			pText.setStyle(Paint.Style.FILL);
			pText.setTextAlign(Paint.Align.LEFT);
			pText.setTextSize(20);
			pText.setTypeface(Typeface.SANS_SERIF);
			pText.setColor(Color.WHITE);
				
		}
		
		canvas.drawBitmap(b, point.getX()-xOff, point.getY()- yOff, null);
		canvas.drawText(point.getTitle(), point.getX() + xOff,point.getY()+8, pText);
		
	}

}