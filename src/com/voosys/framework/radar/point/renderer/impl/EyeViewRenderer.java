package com.voosys.framework.radar.point.renderer.impl;

import java.util.ArrayList;

import com.voosys.spotter.R;
import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;

public class EyeViewRenderer implements PointRenderer {
	
	private Bitmap selectedBitmap = null;
	private Resources resource;
	private int xSelectedOff;
	private int ySelectedOff;
	private Paint pText;
	private Paint pDistance;
	private float fontSize;
	
	public EyeViewRenderer(Resources resource, float size) {
		
		this.resource = resource;
		this.fontSize = size;
		
	}
	
	@Override
	public void drawPoint(Point point, Canvas canvas, Orientation orientation) {
		
		if (point != null) {
			
			selectedBitmap = BitmapFactory.decodeResource(resource, R.drawable.point);
						
			if (selectedBitmap != null) {
			
				xSelectedOff   = selectedBitmap.getWidth() / 2;
				ySelectedOff   = selectedBitmap.getHeight() / 2;		
				pText          = new Paint(Paint.ANTI_ALIAS_FLAG);
				pDistance      = new Paint(Paint.ANTI_ALIAS_FLAG);
				
				pText.setStyle(Paint.Style.STROKE);
				pText.setTextAlign(Paint.Align.LEFT);
				pText.setTextSize((float) this.fontSize);
				pText.setTypeface(Typeface.SANS_SERIF);
				pText.setColor(Color.BLACK);
				pDistance.setStyle(Paint.Style.STROKE);
				pDistance.setTextAlign(Paint.Align.LEFT);
				pDistance.setTextSize((float) this.fontSize - 1);
				pDistance.setTypeface(Typeface.SANS_SERIF);
				pDistance.setColor(Color.BLACK);
				
				canvas.drawBitmap(selectedBitmap, point.getX() - xSelectedOff, point.getY() - ySelectedOff, null);
							
				String title  = point.getTitle();
				long distance = Math.round(point.getDistance());
						
				if (title.length() > 20) {
					
					canvas.drawText(title.substring(0, 20) + "...", point.getX() - 75, point.getY()-5, pText);
					
				} else {
					
					canvas.drawText(title, point.getX() - 75, point.getY()-5, pText);
					
				}
					
				if (distance >= 1000) {
					
					canvas.drawText(String.valueOf(distance / 1000) + " Km", point.getX() - 75, point.getY()+20, pDistance);
					
				} else {
				
					canvas.drawText(String.valueOf(distance) + " M", point.getX() - 75, point.getY()+20, pDistance);
					
				}
				
			}
	
		}
		
	}
	
}