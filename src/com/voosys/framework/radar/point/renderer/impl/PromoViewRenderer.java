package com.voosys.framework.radar.point.renderer.impl;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class PromoViewRenderer implements PointRenderer {
	
	private Bitmap selectedBitmap = null;
	private Resources res;
	private int selectedId;
	private int xSelectedOff;
	private int ySelectedOff;
	private int unselectedId;
	private Bitmap unselectedBitmap;
	private int xUnselectedOff;
	private int yUnselectedOff;
	
	public PromoViewRenderer(Resources res, int selectedId, int unselectedId) {
		
		this.selectedId   = selectedId;
		this.unselectedId = unselectedId;
		this.res          = res;
		
	}
	
	@Override
	public void drawPoint(Point point, Canvas canvas, Orientation orientation) {
		
		if (selectedBitmap == null) {
			
			selectedBitmap   = BitmapFactory.decodeResource(res, selectedId);
			unselectedBitmap = BitmapFactory.decodeResource(res, unselectedId);			
			xSelectedOff     = selectedBitmap.getWidth() / 2;
			ySelectedOff     = selectedBitmap.getHeight() / 2;		
			xUnselectedOff   = unselectedBitmap.getWidth() / 2;
			yUnselectedOff   = unselectedBitmap.getHeight() / 2;
						
		}
		
		if (!point.isHidden()) {
		
			if (point.isSelected()) {
				
				canvas.drawBitmap(selectedBitmap, point.getX() - xSelectedOff, point.getY() - ySelectedOff, null);
				
			} else {
				
				canvas.drawBitmap(unselectedBitmap, point.getX() - xUnselectedOff, point.getY() - yUnselectedOff, null);
				
			}
		
		}
		
	}
	
}