package com.voosys.framework.radar.point.renderer;

import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.point.Point;

import android.graphics.Canvas;

public interface PointRenderer {
	
	/*
	* drawPoint method
	* @param Point point, Canvas canvas, Orientation orientation
	* @return NULL
	* @description drawPoint method
	*/
	public void drawPoint(Point point, Canvas canvas, Orientation orientation);
	
}