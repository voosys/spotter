package com.voosys.framework.radar.location;

import java.util.List;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class LocationReader implements LocationListener {
	
	private LocationManager locManager;
	private Context context;
	private boolean gpsProviderActive     = false;
	private boolean networkProviderActive = false;
	private OnLocationObtainedListener onLocationObtainedListener;
	private OnNoProvidersListener onNoProvidersListener;

	public LocationReader(Context context) {
		
		this.context = context;
		
	}

	public LocationReader(Context context, boolean start) {
		
		this.context = context;
		
		if (start) {
			
			start();
			
		}

	}

	public void start() {
		
		gpsProviderActive     = true;
		networkProviderActive = true;
		
		loadLocationManager();
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 250, this);
		locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 300000, 250, this);
		
	}

	private void loadLocationManager() {
		
		if (locManager == null) {
			
			locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			
		}
		
	}

	public void stop() {
		
		locManager.removeUpdates(this);
		
	}

	public boolean isGpsProviderActive() {
		
		return gpsProviderActive;
		
	}

	private void setGpsProviderActive(boolean gpsProviderActive) {
		
		this.gpsProviderActive = gpsProviderActive;
		
	}

	public boolean isNetworkProviderActive() {
		
		return networkProviderActive;
		
	}

	private void setNetworkProviderActive(boolean networkProviderActive) {
		
		this.networkProviderActive = networkProviderActive;
		
	}

	public OnNoProvidersListener getOnNoProvidersListener() {
		
		return onNoProvidersListener;
		
	}

	public void setOnNoProvidersListener(OnNoProvidersListener onNoProvidersListener) {
		
		this.onNoProvidersListener = onNoProvidersListener;
		
	}

	public OnLocationObtainedListener getOnLocationObtainedListener() {
		
		return onLocationObtainedListener;
		
	}

	public void setOnLocationObtainedListener(OnLocationObtainedListener onLocationObtainedListener) {
		
			this.onLocationObtainedListener = onLocationObtainedListener;
			
	}

	@Override
	public void onLocationChanged(Location loc) {
		
		if (onLocationObtainedListener != null) {
			
			onLocationObtainedListener.onLocationObtained(loc);
			
		}

	}

	public Location getLastKnownLocation() {
		
		loadLocationManager();
		
		List<String> providers = locManager.getProviders(true);

		Location lastLocation = null;

		for (int i = providers.size() - 1; i >= 0 && lastLocation==null; i--) {
			
			lastLocation = locManager.getLastKnownLocation(providers.get(i));
			
		}

		return lastLocation;
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		
		if (LocationManager.GPS_PROVIDER.equals(provider)) {
			
			setGpsProviderActive(false);
			
		}
		
		if (LocationManager.NETWORK_PROVIDER.equals(provider)) {
			
			setNetworkProviderActive(false);
			
		}
		
		if (!isNetworkProviderActive() && !isGpsProviderActive() && onNoProvidersListener != null) {
			
			onNoProvidersListener.onNoProviders();
			
		}
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
		if (LocationManager.GPS_PROVIDER.equals(provider)) {
			
			setGpsProviderActive(true);
			
		}
		
		if (LocationManager.NETWORK_PROVIDER.equals(provider)) {
			
			setNetworkProviderActive(true);
			
		}
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}

	public interface OnLocationObtainedListener {
		
		public void onLocationObtained(Location loc);
		
	}

	public interface OnNoProvidersListener {
		
		public void onNoProviders();
		
	}

}