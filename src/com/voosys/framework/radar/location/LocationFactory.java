package com.voosys.framework.radar.location;

import android.location.Location;

public class LocationFactory {
	
	/*
	* CreateLocation method
	* @static
	* @param double latitude, double longitude, double altitude
	* @return Location
	*/
	public static Location createLocation(double latitude, double longitude, double altitude) {
		
		Location l = new Location("");
		
		l.setLatitude(latitude);
		l.setLongitude(longitude);
		l.setAltitude(altitude);
		l.setAccuracy(0);
		
		return l;

	}

	/*
	* CreateLocation method
	* @static
	* @param double latitude, double longitude
	* @return Location 
	*/
	public static Location createLocation(double latitude, double longitude) {
		
		return createLocation(latitude, longitude, 0);

	}
	
}