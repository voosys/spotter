package com.voosys.framework.radar.core;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

public class GyroscopeChecker {
    
    private Context mContext;
    private SensorManager mSensorManager;
    
    /*
	* Constructor method
	* @param Context context
	* @return NULL
	*/
    public GyroscopeChecker(Context context) {
    	
        this.mContext = context;
        
    }
    
    /*
	* gyroscopeAvailable method
	* @param NULL
	* @return boolean
	*/
    public boolean gyroscopeAvailable() {
    	
    	mSensorManager = (SensorManager) this.mContext.getSystemService(Context.SENSOR_SERVICE);
        
    	if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
	  
    		return true;
	  
    	} else {
    		
    		return false;
    	
    	} 
          
    }
    
}