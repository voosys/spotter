package com.voosys.framework.radar.core;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class BitmapSet {
	
	private ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();
	
	public BitmapSet() {
		
	}
	
	/*
	* add method
	* @param ArrayList<Bitmap> mPointBitmaps
	* @return NULL
	*/
	public void add(ArrayList<Bitmap> mPointBitmaps) {
		
		if (mPointBitmaps.size() > 0) {
		
			for (int i=0; i< mPointBitmaps.size(); i++) {
				
				this.bitmaps.add(mPointBitmaps.get(i));
				
			}
			
		}
		
	}
	
	/*
	* getBitmaps method
	* @param NULL
	* @return ArrayList<Bitmap>
	*/
	public ArrayList<Bitmap> getBitmaps() {
	
		return this.bitmaps;
	
	}
	
}