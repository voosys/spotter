package com.voosys.framework.radar.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionChecker {
    
    private Context mContext;
     
    public ConnectionChecker(Context context) {
    	
        this.mContext = context;
        
    }
    
    /*
	* connectionAvailable method
	* @param NULL
	* @return boolean
	*/
    public boolean connectionAvailable() {
    	
        ConnectivityManager mConnectivity = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        
          if (mConnectivity != null) {
        	  
              NetworkInfo[] info = mConnectivity.getAllNetworkInfo();
              
              if (info != null) {
            	  
                  for (int i = 0; i < info.length; i++) {
                	  
                      if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                    	  
                          return true;
                          
                      }
                      
                  }
 
              }
              
          }
          
          return false;
          
    }
    
}