package com.voosys.framework.radar.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {
	
	public static class JsonEntry {
    	
    	public int id;
    	public String title;
    	public String type;
    	public String url;
    	public double latitude;
        public double longitude;
        public String priority;
        public double distance;
        
        private JsonEntry(int id, String title, String type, String url, double lat, double lng, String priority, double distance) {
        	
        	this.id        = id;
        	this.title     = title;
        	this.type      = type;
        	this.url       = url;
        	this.latitude  = lat;
            this.longitude = lng;
            this.priority  = priority;
            this.distance  = distance;
        }
        
    }
    
    /*
	* parse method
	* @param String url
	* @return List<JsonEntry>
	*/
    public List<JsonEntry> parse(String url) throws JSONException, IOException, Exception {
    	
    	StringBuilder jsonResponse = new StringBuilder();
    	URLConnection cnn          = null;
    	BufferedReader in          = null;
    	String inputLine           = "";
    	List<JsonEntry> entries    = new ArrayList<JsonEntry>();
    	    	
    	cnn = new URL(url).openConnection();
	    in  = new BufferedReader(new InputStreamReader(cnn.getInputStream()));
	    
	    while ((inputLine = in.readLine()) != null) {
	    	
	    	jsonResponse.append(inputLine + "\n");
	    	
	    }
        	
        if (cnn != null) {
        	
        	cnn = null;
            
        }
                        
    	if (jsonResponse != null) {
    		
    		JSONArray responseArray = new JSONArray(jsonResponse.toString());
			
			for(int i=0; i<responseArray.length(); i++) {
				
				JSONObject responseData = responseArray.getJSONObject(i);
				
				entries.add(new JsonEntry(responseData.getInt("id"), responseData.getString("title"), responseData.getString("type"), responseData.getString("url"), responseData.getDouble("latitude"), responseData.getDouble("longitude"), responseData.getString("priority"), responseData.getDouble("distance")));
				
			}
    		
    	}
    	
    	return entries;
        
    }
    
}