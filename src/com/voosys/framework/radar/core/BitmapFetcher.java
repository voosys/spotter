package com.voosys.framework.radar.core;

import java.io.IOException;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapFetcher {
	
	/*
	* Fetch method
	* @static
	* @param String bitmapURL
	* @return Bitmap
	*/
	public static Bitmap fetch(String bitmapURL) throws IOException {
		
	    Bitmap bitmap      = null;
	    
	    try {
	    	
	    	HttpGet httpRequest              = new HttpGet(URI.create(bitmapURL));
	    	HttpClient httpclient            = new DefaultHttpClient();
	    	HttpResponse response            = (HttpResponse) httpclient.execute(httpRequest);
	    	HttpEntity entity                = response.getEntity();
	    	BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
	    	
	    	bitmap = BitmapFactory.decodeStream(bufHttpEntity.getContent());
	        
	    } catch (IOException e) {
	    	
	        throw e;	        
	        
	    }
		
	    return bitmap;
	    
	}
	
}