package com.voosys.framework.radar.core;

import com.voosys.spotter.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListViewAdapter extends ArrayAdapter<String>{
	
	String[] entryTitles;
	Integer[] entryIcons;
	String[] entryTypes;
	String[] entryDistances;
	Context context;
 
 	public ListViewAdapter(Activity context, Integer[] entryIcons, String[] entryTitles, String[] entryTypes, String[] entryDistances) {
	 
 		super(context, R.layout.listview_entry, entryTitles);
 
 		this.entryTitles    = entryTitles;
 		this.entryIcons     = entryIcons;
 		this.entryTypes     = entryTypes;
 		this.entryDistances = entryDistances;
 		this.context        = context;
 	
 	}
 	
 	@Override
 	public View getView(int position, View convertView, ViewGroup parent) {
 		 		
 		LayoutInflater inflater   = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 		View entryRow             = inflater.inflate(R.layout.listview_entry, null, true); 		
 		ImageView imageView       = (ImageView) entryRow.findViewById(R.id.imageView);
 		TextView textTitleView    = (TextView) entryRow.findViewById(R.id.textTitleView);
 		TextView textTypeView     = (TextView) entryRow.findViewById(R.id.textTypeView);
 		TextView textDistanceView = (TextView) entryRow.findViewById(R.id.textDistanceView);
 		
 		imageView.setImageResource(entryIcons[position]);
 		textTitleView.setText(entryTitles[position]);
 		textTypeView.setText(entryTypes[position]); 		
 		textDistanceView.setText(entryDistances[position].toString());
 		
 		return entryRow;
 		
 	}
 	
}