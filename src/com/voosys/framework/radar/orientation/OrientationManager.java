package com.voosys.framework.radar.orientation;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class OrientationManager implements SensorEventListener {
	
	public static final int MODE_COMPASS        = 0;
	public static final int MODE_AR             = 1;
	private static final float CIRCLE           = (float) (Math.PI * 2);
	private static final float SMOOTH_THRESHOLD = CIRCLE / 6;
	private static final float SMOOTH_FACTOR    = SMOOTH_THRESHOLD / 5;
	private Orientation orientation             = new Orientation();
	private boolean sensorRunning               = false;
	private int axisMode                        = MODE_COMPASS;
	private int firstAxis                       = SensorManager.AXIS_Y;
	private int secondAxis                      = SensorManager.AXIS_MINUS_X;
	private float[] mGravs            		    = new float[3];
	private float[] mGeoMags          		    = new float[3];
	private float[] mOrientation      			= new float[3];
	private float[] mRotationM        			= new float[9];
	private float[] mRemapedRotationM 			= new float[9];
	private boolean mFailed;
	private float lowPassValue;	
	private SensorManager sensorManager;
	private Orientation oldOrientation;
	private OnOrientationChangedListener onOrientationChangeListener;
	float x, y, z;
	
	public OrientationManager(Activity activity) {
		
		startSensor(activity);
		
	}

	public OrientationManager() {

	}

	public void startSensor(Activity activity) {
		
		if (!sensorRunning) {
			
			sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
			sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_UI);
			sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);

			sensorRunning = true;

		}
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		switch (event.sensor.getType()) {
		
			case Sensor.TYPE_ACCELEROMETER:
				
				System.arraycopy(event.values, 0, mGravs, 0, 3);
				
				break;
				
			case Sensor.TYPE_MAGNETIC_FIELD:
				
				for (int i = 0; i < 3; i++) {
					
					mGeoMags[i] = event.values[i];
					
				}
				
				break;
				
			default:
				
				return;
			
		}
		
		if (SensorManager.getRotationMatrix(mRotationM, null, mGravs, mGeoMags)) {
			
			SensorManager.remapCoordinateSystem(mRotationM, firstAxis, secondAxis,mRemapedRotationM);
			SensorManager.getOrientation(mRemapedRotationM, mOrientation);
			onSuccess();
			
		}
		
	}

	public void setAxisMode(int axisMode) {
		
		this.axisMode = axisMode;
		
		if (axisMode==MODE_COMPASS) {
			
			 firstAxis  = SensorManager.AXIS_Y;
			 secondAxis = SensorManager.AXIS_MINUS_X;
		
		}
		
		if (axisMode==MODE_AR) {
			
			 firstAxis  = SensorManager.AXIS_X;
			 secondAxis = SensorManager.AXIS_Z;
			 
		}
		
	}
	
	void onSuccess() {
		
		if (mFailed) {
			
			mFailed = false;
			
		}

		x = mOrientation[1];
		y = mOrientation[0] ;
		z = mOrientation[2] ;
		
		if (oldOrientation == null) {
			
			orientation.setX(x);
			orientation.setY(y);
			orientation.setZ(z);
			
		} else {
			
			orientation.setX(lowPass(x, oldOrientation.getX()));
			orientation.setY(lowPass(y, oldOrientation.getY()));
			orientation.setZ(lowPass(z, oldOrientation.getZ()));
			
		}

		oldOrientation = orientation;

		if (getOnCompassChangeListener() != null) {
			
			getOnCompassChangeListener().onOrientationChanged(orientation);

		}

	}

	public float lowPass(float newValue, float lowValue) {
		
		if (Math.abs(newValue - lowValue) < CIRCLE / 2) {
			
			if (Math.abs(newValue - lowValue) > SMOOTH_THRESHOLD) {
				
				lowPassValue = newValue;
				
			} else {
				
				lowPassValue = lowValue + SMOOTH_FACTOR * (newValue - lowValue);
				
			}
			
		} else {
			
			if (CIRCLE - Math.abs(newValue - lowValue) > SMOOTH_THRESHOLD) {
				
				lowPassValue = newValue;
				
			} else {
				
				if (lowValue > newValue) {
					
					lowPassValue = (lowValue + SMOOTH_FACTOR * ((CIRCLE + newValue - lowValue) % CIRCLE) + CIRCLE) % CIRCLE;
					
				} else {
					
					lowPassValue = (lowValue - SMOOTH_FACTOR * ((CIRCLE - newValue + lowValue) % CIRCLE) + CIRCLE) % CIRCLE;
					
				}
				
			}
			
		}
		
		return lowPassValue;
		
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	public void stopSensor() {
		
		if (sensorRunning) {
			
			sensorManager.unregisterListener(this);
			
			sensorRunning = false;
			
		}
		
	}

	@Override
	protected void finalize() throws Throwable {
		
		super.finalize();
		
		stopSensor();
		
	}

	public OnOrientationChangedListener getOnCompassChangeListener() {
		
		return onOrientationChangeListener;
		
	}

	public void setOnOrientationChangeListener(OnOrientationChangedListener onOrientationChangeListener) {
		
		this.onOrientationChangeListener = onOrientationChangeListener;
		
	}

	public Orientation getOrientation() {
		
		return orientation;
		
	}

	public void setOrientation(Orientation orientation) {
		
		this.orientation = orientation;
		
	}

	public int getAxisMode() {
		
		return axisMode;
		
	}
	
	public interface OnOrientationChangedListener {
		
		public void onOrientationChanged(Orientation orientation);
		
	}
	
	public static int getPhoneRotation(Activity activity) {
		
		return activity.getWindowManager().getDefaultDisplay().getRotation();
		
	}

}