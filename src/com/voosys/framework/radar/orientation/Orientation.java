package com.voosys.framework.radar.orientation;

public class Orientation {

	private float x = 0;
	private float z = 0;
	private float y = 0;
	
	/*
	* getX method
	* @param NULL
	* @return float
	* @description onCreate method
	*/
	public float getX() {
		
		return x;
		
	}
	
	/*
	* setX method
	* @param float x
	* @return NULL
	* @description onCreate method
	*/
	public void setX(float x) {
		
		this.x = x;
		
	}
	
	/*
	* getZ method
	* @param NULL
	* @return float
	* @description onCreate method
	*/
	public float getZ() {
		
		return z;
		
	}
	
	/*
	* setZ method
	* @param float z
	* @return NULL
	* @description onCreate method
	*/
	public void setZ(float z) {
		
		this.z =  z;
		
	}
	
	/*
	* getY method
	* @param NULL
	* @return float
	* @description onCreate method
	*/
	public float getY() {
		
		return y;
		
	}

	/*
	* setY method
	* @param float y
	* @return NULL
	* @description onCreate method
	*/
	public void setY(float y) {
		
		this.y =  y;
		
	}

}