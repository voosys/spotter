/**
* AugMob GooglePlacesXmlParser class
*
* PHP version 5
*
* @category   AugMob
* @package    Spotter
* @author     Original Author <binupaul@augmob.com>
* @copyright  2013-2014 AugMob Solutions LLP
* @license    http://www.augmob.com/license
* @version    1.0.0
* @link       http://augmob.com/studio
* @since      File available since Release 1.0.0 
*/
package com.voosys.spotter;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GooglePlacesXmlParser {

	private static final String ns = null;
	
    public static class GooglePlaceEntry {
    	
        public String title;
        public double latitude;
        public double longitude;
        public String reference;

        private GooglePlaceEntry(String title, double lat, double lng, String reference) {
        	
            this.title     = title;
            this.latitude  = lat;
            this.longitude = lng;
            this.reference = reference;
            
        }
        
    }
    
    List<GooglePlaceEntry> entries = new ArrayList<GooglePlaceEntry>();
    double mLatitude  = 0.0;
    double mLongitude = 0.0;
        
    public List<GooglePlaceEntry> parse(InputStream in) throws XmlPullParserException, IOException {
    	
        try {
        	
            XmlPullParser parser = Xml.newPullParser();
            
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            
            return readFeed(parser);
            
        } finally {
        	
            in.close();
            
        }
        
    }

    private List<GooglePlaceEntry> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
    	        
        parser.require(XmlPullParser.START_TAG, ns, "PlaceSearchResponse");
        
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("result")) {
            	
                entries.add(readEntry(parser));
                
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        return entries;
        
    }

    private GooglePlaceEntry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "result");
        
        String title     = "";
        double latitude  = 0.0;
        double longitude = 0.0;
        String reference = "";
                
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("name")) {
            	
                title = readTitle(parser);
                
            } else if (name.equals("geometry")) {
            	
            	readGeometry(parser);
            	
            } else if (name.equals("reference")) {
            	
            	reference = readReference(parser);
            
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        latitude  = mLatitude;
        longitude = mLongitude;
        
        return new GooglePlaceEntry(title, latitude, longitude, reference);
        
    }

    private void readGeometry(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
    	while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("location")) {
            	
            	readLocation(parser);
            	            	
            }  else {
            	
                skip(parser);
                
            }
        
    	}
    }
    
    private String readReference(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
    	 parser.require(XmlPullParser.START_TAG, ns, "reference");
         
         String reference = readText(parser);
         
         parser.require(XmlPullParser.END_TAG, ns, "reference");
         
         return reference;
         
    }
    
    private void readLocation(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
    	while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("lat")) {
            	
                mLatitude = Double.parseDouble(readLatitude(parser));
                
            } else if (name.equals("lng")) {
            	
            	mLongitude = Double.parseDouble(readLongitude(parser));
            	
            }  else {
            	
                skip(parser);
                
            }
        
    	}
    	
    }
    
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "name");
        
        String title = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "name");
        
        return title;
        
    }
    
    private String readLatitude(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "lat");
        
        String latitude = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "lat");
        
        return latitude;
        
    }
        
	private String readLongitude(XmlPullParser parser) throws IOException, XmlPullParserException {
		
	    parser.require(XmlPullParser.START_TAG, ns, "lng");
	    
	    String longitude = readText(parser);
	    
	    parser.require(XmlPullParser.END_TAG, ns, "lng");
	    
	    return longitude;
	    
	}
        
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        String result = "";
        
        if (parser.next() == XmlPullParser.TEXT) {
        	
            result = parser.getText();
            
            parser.nextTag();           
            
        }
        
        return result;
        
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        if (parser.getEventType() != XmlPullParser.START_TAG) {
        	
            throw new IllegalStateException();
            
        }
        
        int depth = 1;
        
        while (depth != 0) {
        	
            switch (parser.next()) {
            
            	case XmlPullParser.END_TAG:
            		
                    depth--;
                    
                    break;
                    
            	case XmlPullParser.START_TAG:
            		
                    depth++;
                    
                    break;
                    
            }
            
        }
        
    }
    
}