/**
* AugMob GooglePlaceDetailsXmlParser class
*
* PHP version 5
*
* @category   AugMob
* @package    Spotter
* @author     Original Author <binupaul@augmob.com>
* @copyright  2013-2014 AugMob Solutions LLP
* @license    http://www.augmob.com/license
* @version    1.0.0
* @link       http://augmob.com/studio
* @since      File available since Release 1.0.0 
*/
package com.voosys.spotter;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GooglePlaceDetailsXmlParser {

	private static final String ns = null;
	
    public static class GooglePlaceDetailsEntry {
    	
    	public String title;    	
        public String url;

        private GooglePlaceDetailsEntry(String title, String url) {
        	
        	this.title = title;
            this.url   = url;
            
        }
        
    }
    
    List<GooglePlaceDetailsEntry> entries = new ArrayList<GooglePlaceDetailsEntry>();
        
    public List<GooglePlaceDetailsEntry> parse(InputStream in) throws XmlPullParserException, IOException {
    	
        try {
        	
            XmlPullParser parser = Xml.newPullParser();
            
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            
            return readFeed(parser);
            
        } finally {
        	
            in.close();
            
        }
        
    }

    private List<GooglePlaceDetailsEntry> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
    	        
        parser.require(XmlPullParser.START_TAG, ns, "PlaceDetailsResponse");
        
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("result")) {
            	
                entries.add(readEntry(parser));
                
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        return entries;
        
    }

    private GooglePlaceDetailsEntry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "result");
        
        String title   = "";
        String url     = "";
                
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("name")) {
            	
                title = readTitle(parser);
                
            } else if (name.equals("url")) {
            	
            	url = readUrl(parser);
                
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        return new GooglePlaceDetailsEntry(title, url);
        
    }
        
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "name");
        
        String title = readText(parser);
        
        Log.w("Title: ", title);
        
        parser.require(XmlPullParser.END_TAG, ns, "name");
        
        return title;
        
    }
    
    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "url");
        
        String url = readText(parser);
        
        Log.w("Url: ", url);
        
        parser.require(XmlPullParser.END_TAG, ns, "url");
        
        return url;
        
    }
    
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        String result = "";
        
        if (parser.next() == XmlPullParser.TEXT) {
        	
            result = parser.getText();
            
            parser.nextTag();           
            
        }
        
        return result;
        
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        if (parser.getEventType() != XmlPullParser.START_TAG) {
        	
            throw new IllegalStateException();
            
        }
        
        int depth = 1;
        
        while (depth != 0) {
        	
            switch (parser.next()) {
            
            	case XmlPullParser.END_TAG:
            		
                    depth--;
                    
                    break;
                    
            	case XmlPullParser.START_TAG:
            		
                    depth++;
                    
                    break;
                    
            }
            
        }
        
    }
    
}