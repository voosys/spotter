package com.voosys.spotter;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {

	private WebView webView;
	
	/*
	* onCreate method
	* @param Bundle savedInstanceState
	* @return NULL
	*/
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		String mUrl   = "";
		Bundle extras = getIntent().getExtras();
		webView       = new WebView(this);
		
		webView.setWebViewClient(new WebViewClient() {
			
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            	
                // Do nothing
                
            }
            
        });
		
		if (extras != null) {
			
		    mUrl = extras.getString("URL");
		    
		    webView.loadUrl(mUrl);
		    webView.getSettings().setJavaScriptEnabled(true);
		    setContentView(webView);
		    
		}

	}

}