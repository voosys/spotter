/**
* AugMob PointsModel class
*
* PHP version 5
*
* @category   AugMob
* @package    Spotter
* @author     Original Author <binupaul@augmob.com>
* @copyright  2013-2014 AugMob Solutions LLP
* @license    http://www.augmob.com/license
* @version    1.0.0
* @link       http://augmob.com/studio
* @since      File available since Release 1.0.0 
*/
package com.voosys.spotter;

import java.util.ArrayList;
import java.util.List;

import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.renderer.PointRenderer;

public class PointsModel {
	
	private List<Point> mPoints;
	
	public PointsModel() {
		
		mPoints = new ArrayList<Point>();
		
	}
	
	public List<Point> getPoints(PointRenderer renderer) {
		
		return mPoints;

	}
	
	public List<Point> setPoints(PointRenderer renderer, ArrayList<Point> points) {
	
		mPoints = points;
		
		return mPoints;
		
	}
}
