package com.voosys.spotter;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.voosys.spotter.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.voosys.framework.radar.core.BitmapSet;
import com.voosys.framework.radar.core.ConnectionChecker;
import com.voosys.framework.radar.core.GyroscopeChecker;
import com.voosys.framework.radar.core.JsonParser;
import com.voosys.framework.radar.core.ListViewAdapter;
import com.voosys.framework.radar.core.JsonParser.JsonEntry;
import com.voosys.framework.radar.location.LocationFactory;
import com.voosys.framework.radar.orientation.Orientation;
import com.voosys.framework.radar.orientation.OrientationManager;
import com.voosys.framework.radar.orientation.OrientationManager.OnOrientationChangedListener;
import com.voosys.framework.radar.point.Point;
import com.voosys.framework.radar.point.impl.SimplePoint;
import com.voosys.framework.radar.point.renderer.PointRenderer;
import com.voosys.framework.radar.point.renderer.impl.EyeViewRenderer;
import com.voosys.framework.radar.point.renderer.impl.SimplePointRenderer;
import com.voosys.framework.radar.view.CameraView;
import com.voosys.framework.radar.view.EyeView;
import com.voosys.framework.radar.view.RadarView;
import com.voosys.framework.radar.view.ARView.OnPointPressedListener;
import com.voosys.spotter.GeoNamesXmlParser;
import com.voosys.spotter.GooglePlacesXmlParser;
import com.voosys.spotter.GeoNamesXmlParser.GeoNameEntry;
import com.voosys.spotter.GooglePlaceDetailsXmlParser.GooglePlaceDetailsEntry;
import com.voosys.spotter.GooglePlacesXmlParser.GooglePlaceEntry;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

public class Spotter extends Activity implements OnOrientationChangedListener, OnPointPressedListener, OnMyLocationButtonClickListener, OnMarkerClickListener, OnMapClickListener, OnItemClickListener, OnConnectionFailedListener, ConnectionCallbacks, LocationListener {

	private static final String mServerURL   	   = "http://www.voosys.com/test/spotter/points.php";   
    private static final int MAX_DISTANCE    	   = 100000000;
	private static final String SPOTTER_VIEW   	   = "Spotter View";
	private static final String MAP_VIEW     	   = "Map View";
	private static final String LIST_VIEW    	   = "List View";
	private double latitude                  	   = 0;
	private double longitude                 	   = 0;
	private int    scanRadius                	   = 5;
	private PointRenderer arRenderer;
	private PointRenderer crRenderer;
	private EyeView ar;
	private RadarView cv;
	private CameraView camera;
	private GoogleMap mMap;
	private LinearLayout base;
	private View mARView;
	private View mMapView;
	private View mListView;	
	private FrameLayout cameraFrame;
	private OrientationManager compass;
	private Location location;
	private LocationManager locationManager;
	private ConnectionChecker mConnectionChecker;
	private GyroscopeChecker mGyroscopeChecker;
	private TelephonyManager mTelephonyManager;
	private Criteria criteria;
	private String mActiveMode;
	private String provider;
	private String mJsonURL;
	private List<Object> pointsList         = null;
	private List<Marker> markersList        = null;
	private List<Point> points              = null;
	private List<Point> cpoints             = null;
	private ArrayList<Point> geoNamePoints  = new ArrayList<Point>();
	private ArrayList<Point> geoNameCPoints = new ArrayList<Point>();
	private List<JsonEntry> mJsonEntries    = null;
	private ArrayList<Bitmap> mPointBitmaps = new ArrayList<Bitmap>();
	private BitmapSet bitmaps               = new BitmapSet();
	private static long backPressCounter;
		 
	private class JsonEntriesTask extends AsyncTask<String, String, String> {
		
		@Override
        protected String doInBackground(String... urls) {
        	
            try {
            	
            	mJsonEntries = loadJsonEntries(urls[0]);  	
            	
            	return "Success";
                
            } catch (JSONException e) {
            	
            	return "Failed";
                
            } catch (IOException e) {
            	
            	return "Failed";
            	
            } catch (Exception e) {
            	
            	return "Failed";
                
            }
            
        }

        @Override
        protected void onPostExecute(String result) {        	
        	
        	setEntries();
        	
        }
        
    }
	
	private List<JsonEntry> loadJsonEntries(String urlString) throws JSONException, IOException, Exception {
    	
    	List<JsonEntry> jsonEntries = null;
        JsonParser jsonParser       = new JsonParser();

        jsonEntries = jsonParser.parse(urlString);
        
        return jsonEntries;
        
    }
	
	/*
	* initiateRadar method
	* @param NULL
	* @return NULL
	* @description initiateRadar method
	*/
	private void initiateRadar() {
		
	    ar          = (EyeView) findViewById(R.id.augmentedView1);
		cv          = (RadarView) findViewById(R.id.radarView1);
		mActiveMode = SPOTTER_VIEW;		
		arRenderer  = new EyeViewRenderer(getResources(), getResources().getDimension(R.dimen.textsize));
		crRenderer  = new SimplePointRenderer();
		points      = new PointsModel().getPoints(arRenderer);
		cpoints     = new PointsModel().getPoints(crRenderer);
		
		ar.setMaxDistance(MAX_DISTANCE);
		cv.setMaxDistance(MAX_DISTANCE);		
		ar.setOnPointPressedListener(this);
		cv.setOnPointPressedListener(this);
		ar.setPoints(points);
		ar.setPosition(LocationFactory.createLocation(latitude, longitude, 0));
		ar.setOnPointPressedListener(this);
		cv.setPoints(cpoints);
		cv.setPosition(LocationFactory.createLocation(latitude, longitude, 0));
		cv.setRotableBackground(R.drawable.arrow);
		
		cameraFrame = (FrameLayout) findViewById(R.id.cameraFrame);
		camera      = new CameraView(this);
		
		cameraFrame.addView(camera);
		
		getResources().getDimension(R.dimen.textsize);
		
	}
	
	/*
	* initiateMap method
	* @param NULL
	* @return NULL
	* @description initiateMap method
	*/
	private void initiateMap() {
		
		latitude      = location.getLatitude();
		longitude     = location.getLongitude();
		LatLng latLng = new LatLng(latitude, longitude);
		
		if (mMap == null) {
			
			mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
			
			if (mMap != null) {
	        	
	        	mMap.setOnMarkerClickListener(this);
	        	mMap.setOnMapClickListener(this);
	        	mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
				mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
				
	        }
			
		}
	
	}
	
	/*
	* initiateList method
	* @param NULL
	* @return NULL
	* @description initiateList method
	*/
	private void initiateList() {
				
	}
	
	/*
	* onSaveInstanceState method
	* @param Bundle outState
	* @return NULL
	* @description onSaveInstanceState method
	*/
	@Override
    protected void onSaveInstanceState(Bundle outState) {
        
		outState.putString("SAVED_MODE", mActiveMode);
		
		super.onSaveInstanceState(outState);
		
    }
	
	/*
	* onCreate method
	* @param Bundle savedInstanceState
	* @return NULL
	* @description onCreate method
	*/
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		locationManager    = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		criteria           = new Criteria();
	    provider           = locationManager.getBestProvider(criteria, false);
	    location           = locationManager.getLastKnownLocation(provider);	    
	    mConnectionChecker = new ConnectionChecker(getApplicationContext());
	    mGyroscopeChecker  = new GyroscopeChecker(getApplicationContext());
	    compass            = new OrientationManager(this);
	    pointsList         = new ArrayList<Object>();
		mTelephonyManager  = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
				
		super.onCreate(savedInstanceState);
		
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 600000, 10000, this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 600000, 10000, this);
		compass.setAxisMode(OrientationManager.MODE_AR);
		compass.setOnOrientationChangeListener(this);
		compass.startSensor(this);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.base);
		
		base    = (LinearLayout)this.findViewById(R.id.base_container);
		mARView = getLayoutInflater().inflate(R.layout.ar, null);
		
		base.addView(mARView);
		
		initiateRadar();
		
		if (location != null) {
	    	
	    	onLocationChanged(location);
	    	
	    } else {
	    	
	    	AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Spotter.this);
			
			dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				
		           public void onClick(DialogInterface dialog, int id) {
		           
		        	   Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		        	   
		        	   startActivity(intent);
		        	   
		           }
		           
		       });
			
			dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				
		           public void onClick(DialogInterface dialog, int id) {
		        	   
		           }
		           
		       });
		
			
			AlertDialog dialogError = dialogBuilder.create();
	        
	        dialogError.setMessage(getString(R.string.LOCATION_NOT_AVAILABLE));                
	        dialogError.show();
	    	
	    }
		
		if (!mConnectionChecker.connectionAvailable()) {
			
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Spotter.this);
			
			dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				
		           public void onClick(DialogInterface dialog, int id) {
		           
		        	   Intent intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
		        	   
		        	   startActivity(intent);
		        	   
		           }
		           
		    });
			
			dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				
		           public void onClick(DialogInterface dialog, int id) {
		        	   		              
		           }
		           
		    });
		
			
			AlertDialog dialogError = dialogBuilder.create();
	        
	        dialogError.setMessage(getString(R.string.NETWORK_NOT_AVAILABLE));                
	        dialogError.show();
			
		}
		
		if (!mGyroscopeChecker.gyroscopeAvailable()) {
			
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Spotter.this);
			AlertDialog dialogError           = dialogBuilder.create();
	        			
			dialogError.setTitle(R.string.app_name);
	        dialogError.setMessage(getString(R.string.GYROSCOPE_SENSOR_NOT_AVAILABLE));    
	        dialogBuilder.setPositiveButton(R.string.ok, null);
	        dialogError.setCancelable(true);
	        dialogError.show();
			
		}
		
	}
	
	/*
	* onConfigurationChanged method
	* @param Configuration newConfig
	* @return NULL
	* @description onConfigurationChanged method
	*/	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		
		super.onConfigurationChanged(newConfig);
		
	}
	
	/*
	* onPause method
	* @param NULL
	* @return NULL
	* @description onPause method
	*/
	@Override
	protected void onPause() {
		
		super.onPause();
		
		compass.stopSensor();
		locationManager.removeUpdates(this);
				
	}
	
	/*
	* onResume method
	* @param NULL
	* @return NULL
	* @description onResume method
	*/
	@Override
	protected void onResume() {
		
		super.onResume();
		
		compass.startSensor(this);	
		
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 400, 1, this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400, 1, this);

	}
	
	/*
	* onOrientationChanged method
	* @param Orientation orientation
	* @return NULL
	* @description onOrientationChanged method
	*/
	@Override
	public void onOrientationChanged(Orientation orientation) {
		
		int radius = cv.getScanRadius();
		
		if (scanRadius != radius) {
			
			scanRadius = radius;
			
			setDeviceScanRadius();
			
		}
		
		ar.setOrientation(orientation);
		ar.setPhoneRotation(OrientationManager.getPhoneRotation(this));
		cv.setOrientation(orientation);
		
	}
	
	/*
	* onPointPressed method
	* @param Point point
	* @return NULL
	* @description onPointPressed method
	*/
	@Override
	public void onPointPressed(final Point point) {
		
		openEntry(point);		
				
	}
	
	/*
	* onMarkerClick method
	* @param Marker m
	* @return boolean
	* @description onMarkerClick method
	*/
	@Override
	public boolean onMarkerClick(Marker m) {
		
		int index       = 0;
		int markerIndex = -1;
		int pointIndex  = 0;
		
		if (pointsList != null) {
			
			for (Object marker : markersList) {
				
				if (marker.hashCode() == m.hashCode()) {
					
					markerIndex = index;
					
					break;					
					
				}
				
				++index;
				
			}
			
			index = 0;
			
			if (pointsList != null) {
				
				for (Object point : pointsList) {
					
					if (index == markerIndex) {
						
						openEntry(point);
						
						break;
						
					}
					
					++index;
					
				}
				
			}
			
		}
						
		return true;
		
	}
	
	/*
	* onMyLocationButtonClick method
	* @param NULL
	* @return boolean
	* @description onMyLocationButtonClick method
	*/
	@Override
	public boolean onMyLocationButtonClick() {
		
		return false;
		
	}
	
	/*
	* onMapClick method
	* @param LatLng point
	* @return NULL
	* @description onMapClick method
	*/
	public void onMapClick(LatLng point) {
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		
		int index = 0;
		
		if (pointsList != null) {
			
			for (Object point : pointsList) {
				
				if (index == arg2) {
					
					openEntry(point);
					
					break;
					
				}
				
				++index;
				
			}
			
		}
		
	}
	
	/*
	* onLocationChanged method
	* @param Location location
	* @return NULL
	* @description onLocationChanged method
	*/
	@Override
	public void onLocationChanged(Location location) {
				
		double latitude  = Math.floor(location.getLatitude()  * 1000000 + .5) / 1000000;
		double longitude = Math.floor(location.getLongitude() * 1000000 + .5) / 1000000;
		
		//latitude  = 25.3328641;
		//longitude = 51.459619;
		
		setDeviceLocation(latitude, longitude);
		
	}
	
	/*
	* onStatusChanged method
	* @param String provider, int status, Bundle extras
	* @return NULL
	* @description onStatusChanged method
	*/
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	
	}
	
	/*
	* onProviderEnabled method
	* @param String provider
	* @return NULL
	* @description onProviderEnabled method
	*/
	@Override
	public void onProviderEnabled(String provider) {
	
	}
	
	/*
	* onProviderDisabled method
	* @param String provider
	* @return NULL
	* @description onProviderDisabled method
	*/
	@Override
	public void onProviderDisabled(String provider) {
				
	}
	
	/*
	* unselectAllPoints method
	* @param NULL
	* @return NULL
	* @description unselectAllPoints method
	*/
	public void unselectAllPoints() {
		
		for (Point point: points) {
			
			point.setSelected(false);
			
		}
		
	}
	
	/*
	* setEntries method
	* @param NULL
	* @return NULL
	* @description setEntries method
	*/
	public void setEntries() {
		
		pointsList.clear();
		
		if (mJsonEntries != null) {
			
			for (JsonEntry entry : mJsonEntries) {
				
				pointsList.add(entry);
				
			}
			
		}
		
		setRadarEntries();
		
	}
	
	/*
	* setRadarEntries method
	* @param NULL
	* @return NULL
	* @description setRadarEntries method
	*/
	public void setRadarEntries() {
		
		mPointBitmaps.clear();
		
		if (pointsList != null) {
			
			for (Object point : pointsList) {		
				
				if (point instanceof JsonEntry) {
					
					JsonEntry entry = (JsonEntry) point;
					
					geoNamePoints.add(new SimplePoint(1, LocationFactory.createLocation(entry.latitude, entry.longitude, 0), arRenderer, entry.title, entry.type, entry.url, entry.distance, ""));
					geoNameCPoints.add(new SimplePoint(1, LocationFactory.createLocation(entry.latitude, entry.longitude, 0), crRenderer, entry.title, entry.type, entry.url, entry.distance, ""));
										
				}
				
			}
			
			plotPOIEntries();
			
		}		
		
	}
	
	/*
	* plotPOIEntries method
	* @param NULL
	* @return NULL
	* @description plotPOIEntries method
	*/
	public void plotPOIEntries() {
		
		bitmaps = new BitmapSet();
		
		bitmaps.add(mPointBitmaps);
		
		PointRenderer arRenderer = new EyeViewRenderer(getResources(), getResources().getDimension(R.dimen.textsize));		
		points                   = new PointsModel().setPoints(arRenderer, geoNamePoints);
		cpoints                  = new PointsModel().setPoints(crRenderer, geoNameCPoints);
		
		ar.setPoints(points);
		ar.setPosition(LocationFactory.createLocation(latitude, longitude, 0));
		cv.setPoints(cpoints);
		cv.setPosition(LocationFactory.createLocation(latitude, longitude, 0));	
		
	}
	
	/*
	* setMapEntries method
	* @param NULL
	* @return NULL
	* @description setMapEntries method
	*/
	public void setMapEntries() {
		
		if (pointsList != null) {
			
			markersList = new ArrayList<Marker>();
			
			for (Object point : pointsList) {
								
				if (point instanceof JsonEntry) {
					
					JsonEntry entry = (JsonEntry) point;
					
					if (mMap != null) {
					
						Marker mMarker;
						
						mMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(entry.latitude, entry.longitude)).title(entry.title).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
						
						markersList.add(mMarker);
						
					}
					
				}
				
			}
			
		}
		
	}
	
	/*
	* setListEntries method
	* @param NULL
	* @return NULL
	* @description setListEntries method
	*/
	public void setListEntries() {
		
		ListView listView                = (ListView) findViewById(R.id.listView);
		List<String> entryList           = new ArrayList<String>();
		List<String> entryDistanceList   = new ArrayList<String>();
		List<String> entryTypeList       = new ArrayList<String>();
		ArrayList<Integer> entryIconList = new ArrayList<Integer>();
		
		if (pointsList != null) {
			
			for (Object point : pointsList) {
				
				if (point instanceof JsonEntry) {
					
					JsonEntry entry = (JsonEntry) point;
					
					entryList.add(entry.title);		
					entryTypeList.add(entry.type);
					entryDistanceList.add(entry.distance + " M");					
					entryIconList.add(R.drawable.icon);
					
				}
				
			}
			
		}
		
		String[] entryTitles    = Arrays.copyOf(entryList.toArray(), entryList.toArray().length, String[].class);
		String[] entryTypes     = Arrays.copyOf(entryTypeList.toArray(), entryTypeList.toArray().length, String[].class);
		String[] entryDistances = Arrays.copyOf(entryDistanceList.toArray(), entryDistanceList.toArray().length, String[].class);
		Integer entryIcons[]    = Arrays.copyOf(entryIconList.toArray(), entryIconList.toArray().length, Integer[].class);		
		ListViewAdapter adapter = new ListViewAdapter(this, entryIcons, entryTitles, entryTypes, entryDistances);
		
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		
	}
	
	/*
	* openEntry method
	* @param Point point
	* @return NULL
	* @description openEntry method
	*/
	public void openEntry(final Point point) {

		final Dialog dialog = new Dialog(this);
		
		unselectAllPoints();
		point.setSelected(true);
		
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.actions);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		
		TextView entryTitle = (TextView) dialog.findViewById(R.id.entryTitle);
		TextView readMore   = (TextView) dialog.findViewById(R.id.readMore);
		TextView viewRoute  = (TextView) dialog.findViewById(R.id.viewRoute);
		
		entryTitle.setText(point.getTitle());
		
		readMore.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (point.getUrl().length() > 0) {
					
					Intent webViewActivity = new Intent(getApplicationContext(), WebViewActivity.class);
					
					webViewActivity.putExtra("URL", point.getUrl());
					startActivity(webViewActivity);					
					dialog.dismiss();
					
				}
				
			}

		});	
		
		viewRoute.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (point.getUrl().length() > 0 || point.getReference().length() > 0) { 
					
					String routeURL = "https://maps.google.com/maps?daddr=" + point.getLocation().getLatitude() + "," + point.getLocation().getLongitude() + "&saddr=" + latitude + "," + longitude;
					
					Intent webViewActivity = new Intent(getApplicationContext(), WebViewActivity.class);
					
					webViewActivity.putExtra("URL", routeURL);
					startActivity(webViewActivity);					
					dialog.dismiss();
					
				}
				
			}

		});
		
	}
	
	/*
	* openEntry method
	* @param Object point
	* @return NULL
	* @description openEntry method
	*/
	public void openEntry(final Object point) {
		
		final Dialog dialog   = new Dialog(this);
		JsonEntry entry = null;
		
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.actions);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		
		TextView  entryTitle = (TextView) dialog.findViewById(R.id.entryTitle);
		TextView readMore    = (TextView) dialog.findViewById(R.id.readMore);
		TextView viewRoute   = (TextView) dialog.findViewById(R.id.viewRoute);
		
		if (point instanceof JsonEntry) {
			
			final JsonEntry _entry;
			
			entry = (JsonEntry) point;
			
			_entry = entry;
			
			entryTitle.setText(entry.title);
			
			readMore.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					if (_entry.url.length() > 0) {
						
						Intent webViewActivity = new Intent(getApplicationContext(), WebViewActivity.class);
						
						webViewActivity.putExtra("URL", _entry.url);
						startActivity(webViewActivity);					
						dialog.dismiss();
						
					}
					
				}

			});	
			
			viewRoute.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					
					if (_entry.url.length() > 0) { 
						
						String routeURL = "https://maps.google.com/maps?daddr=" + _entry.latitude + "," + _entry.longitude + "&saddr=" + latitude + "," + longitude;
						
						Intent webViewActivity = new Intent(getApplicationContext(), WebViewActivity.class);
						
						webViewActivity.putExtra("URL", routeURL);
						startActivity(webViewActivity);					
						dialog.dismiss();
						
					}
					
				}

			});
		
		}
		
	}
	
	/*
	* setDeviceLocation method
	* @param double lat, double lng
	* @return NULL
	* @description setDeviceLocation method
	*/
	public void setDeviceLocation(double lat, double lng) {	
		
		if (latitude != lat && longitude != lng) {
			
			latitude     = lat;
			longitude    = lng;
			pointsList   = new ArrayList<Object>();
		    mJsonEntries = null;
		    mJsonURL     = mServerURL + "?latitude=" + latitude + "&longitude=" + longitude + "&radius="+scanRadius;
			
		    new JsonEntriesTask().execute(mJsonURL);
			
		}
		
	}
	
	/*
	* setDeviceScanRadius method
	* @param NULL
	* @return NULL
	* @description setDeviceScanRadius method
	*/
	public void setDeviceScanRadius() {
		
		pointsList   = new ArrayList<Object>();
	    mJsonEntries = null;
	    mJsonURL     = mServerURL + "?latitude=" + latitude + "&longitude=" + longitude + "&radius="+scanRadius;
		
	    new JsonEntriesTask().execute(mJsonURL);
		
	}
	
	public boolean onPrepareOptionsMenu(Menu menu) 
    {
        super.onPrepareOptionsMenu(menu);
        
        menu.clear();

        if(mActiveMode == SPOTTER_VIEW) {
        	
            //menu.add("Map View");
            menu.add("List View");
            
        } else if(mActiveMode == MAP_VIEW) {
        	
            //menu.add("Spotter View");
            //menu.add("List View");
            
        } else if(mActiveMode == LIST_VIEW) {
        	
            menu.add("Spotter View");
            //menu.add("Map View");
            
        }
        
        return true;
        
    }
	
	/*
	* getActiveMode method
	* @param NULL
	* @return String
	* @description getActiveMode method
	*/
	public String getActiveMode() {
		
		return mActiveMode;
	
	}
	
	/*
	* setActiveMode method
	* @param String mode
	* @return NULL
	* @description setActiveMode method
	*/
	public void setActiveMode(String mode) {
		
		mActiveMode = mode;
		
		setActiveView(mode);
		
	}
	
	/*
	* setActiveView method
	* @param String mode
	* @return NULL
	* @description setActiveView method
	*/
	public void setActiveView(String mode) {
		
		base = (LinearLayout)this.findViewById(R.id.base_container);
		
		if (mode.equals(SPOTTER_VIEW)) {
        	
			mARView = getLayoutInflater().inflate(R.layout.ar, null);
						
			base.removeAllViewsInLayout();
			base.addView(mARView);
			initiateRadar();
    		
        	
        } else if (mode.equals(MAP_VIEW)) {
        	
        	if (mMapView == null) {
        		
        		mMapView = getLayoutInflater().inflate(R.layout.map, null);
        		
        	}
    		
        	base.removeAllViewsInLayout();
        	base.addView(mMapView);
    		initiateMap();
    		setMapEntries();
        	
        	
        } else if (mode.equals(LIST_VIEW)) {
        	
        	if (mListView == null) {
        		
        		mListView = getLayoutInflater().inflate(R.layout.listview, null);
        	
        	}        		
    		
    		base.removeAllViewsInLayout();
    		base.addView(mListView);
    		initiateList();
    		setListEntries();
    		
        }
		
	}    		

    public boolean onOptionsItemSelected(MenuItem item) {
    	
        if (item.getTitle().equals(SPOTTER_VIEW)) {
        
        	setActiveMode(SPOTTER_VIEW);
        	
        	
        } else if (item.getTitle().equals(MAP_VIEW)) {
        	
        	setActiveMode(MAP_VIEW);
        	
        	
        } else if (item.getTitle().equals(LIST_VIEW)) {
        
        	setActiveMode(LIST_VIEW);
        	
        }

        return true;
        
    }
    
    public void onBackPressed() {
    	
    	super.onBackPressed();
    	
    }

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		
		// Do nothing
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		
		// Do nothing
		
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}
		
}
