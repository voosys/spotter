/**
* AugMob GeoNamesXmlParser class
*
* PHP version 5
*
* @category   AugMob
* @package    Spotter
* @author     Original Author <binupaul@augmob.com>
* @copyright  2013-2014 AugMob Solutions LLP
* @license    http://www.augmob.com/license
* @version    1.0.0
* @link       http://augmob.com/studio
* @since      File available since Release 1.0.0 
*/
package com.voosys.spotter;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GeoNamesXmlParser {

	private static final String ns = null;
	
    public static class GeoNameEntry {
    	
        public String title;
        public double latitude;
        public double longitude;
        public double elevation;
        public double distance;
        public String url;

        private GeoNameEntry(String title, double lat, double lng, double elevation, double distance, String url) {
        	
            this.title     = title;
            this.latitude  = lat;
            this.longitude = lng;
            this.elevation = elevation;
            this.distance  = distance;
            this.url       = url;
            
        }
        
    }

    public List<GeoNameEntry> parse(InputStream in) throws XmlPullParserException, IOException {
    	
        try {
        	
            XmlPullParser parser = Xml.newPullParser();
            
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            
            return readFeed(parser);
            
        } finally {
        	
            in.close();
            
        }
        
    }

    private List<GeoNameEntry> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        List<GeoNameEntry> entries = new ArrayList<GeoNameEntry>();

        parser.require(XmlPullParser.START_TAG, ns, "geonames");
        
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("entry")) {
            	
                entries.add(readEntry(parser));
                
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        return entries;
        
    }

    private GeoNameEntry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "entry");
        
        String title     = "";
        double latitude  = 0.0;
        double longitude = 0.0;
        double elevation = 0.0;
        double distance  = 0.0;
        String url       = "";
                
        while (parser.next() != XmlPullParser.END_TAG) {
        	
            if (parser.getEventType() != XmlPullParser.START_TAG) {
            	
                continue;
                
            }
            
            String name = parser.getName();
            
            if (name.equals("title")) {
            	
                title = readTitle(parser);
                
            } else if (name.equals("lat")) {
            	
            	latitude = Double.parseDouble(readLatitude(parser));
                
            } else if (name.equals("lng")) {
            	
            	longitude = Double.parseDouble(readLongitude(parser));
                
            } else if (name.equals("elevation")) {
            	
            	elevation = Double.parseDouble(readElevation(parser));
                
            } else if (name.equals("distance")) {
            	
            	distance = Double.parseDouble(readDistance(parser));
                
            } else if (name.equals("wikipediaUrl")) {
            	
            	url = readUrl(parser);
                
            } else {
            	
                skip(parser);
                
            }
            
        }
        
        return new GeoNameEntry(title, latitude, longitude, elevation, distance, url);
        
    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "title");
        
        String title = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "title");
        
        return title;
        
    }
    
    private String readLatitude(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "lat");
        
        String latitude = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "lat");
        
        return latitude;
        
    }


	private String readLongitude(XmlPullParser parser) throws IOException, XmlPullParserException {
		
	    parser.require(XmlPullParser.START_TAG, ns, "lng");
	    
	    String longitude = readText(parser);
	    
	    parser.require(XmlPullParser.END_TAG, ns, "lng");
	    
	    return longitude;
	    
	}
    
    private String readElevation(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "elevation");
        
        String elevation = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "elevation");
        
        return elevation;
        
    }
    
    private String readDistance(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "distance");
        
        String distance = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "distance");
        
        return distance;
        
    }
    
    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        parser.require(XmlPullParser.START_TAG, ns, "wikipediaUrl");
        
        String url = readText(parser);
        
        parser.require(XmlPullParser.END_TAG, ns, "wikipediaUrl");
        
        return url;
        
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
    	
        String result = "";
        
        if (parser.next() == XmlPullParser.TEXT) {
        	
            result = parser.getText();
            
            parser.nextTag();
            
        }
        
        return result;
        
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
    	
        if (parser.getEventType() != XmlPullParser.START_TAG) {
        	
            throw new IllegalStateException();
            
        }
        
        int depth = 1;
        
        while (depth != 0) {
        	
            switch (parser.next()) {
            
            	case XmlPullParser.END_TAG:
            		
                    depth--;
                    
                    break;
                    
            	case XmlPullParser.START_TAG:
            		
                    depth++;
                    
                    break;
                    
            }
            
        }
        
    }
    
}